FROM registry.gitlab.com/prismatic-jd/containers/asdf:0.11.3

# Define a build-time argument (branch name) to be used later in the Dockerfile
ARG CI_COMMIT_REF_NAME

# Set the default shell to bash with options to halt on errors and execute commands passed as arguments
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# Install the AWS CLI plugin for ASDF
RUN asdf plugin add awscli https://github.com/MetricMike/asdf-awscli.git

# Install the appropriate version of AWS CLI based on the branch name
RUN asdf install awscli "$([ "$CI_COMMIT_REF_NAME" == "main" ] && echo "latest" || echo "$CI_COMMIT_REF_NAME")"

# Set the installed AWS CLI version as the global version in the ASDF environment and write it to a .tool-versions file
RUN asdf global awscli "$([ "$CI_COMMIT_REF_NAME" == "main" ] && echo "latest" || echo "$CI_COMMIT_REF_NAME")" && \
    echo "awscli $("${SERVICE_USER_HOME}/.asdf/bin/asdf" current awscli | awk '{print $2}')" > ".tool-versions"
